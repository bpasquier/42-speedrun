#!/bin/bash

# Script pour un chronomètre
# affiche le temps écouler depuis le lancement du script

HD=$(date +%s)
SC=$(date +%S)	#	#	#	# afiche les secondes en cours
tpfx=60	#	#	#	#	# 60
let csc=$tpfx-$SC	#	#	# calcul les secondes en cours jusqu'à 60 secondes

while : ;do
clear
HC=$(date +%s)
let chronoH=($HC-$HD)/3600 # affiche des heures
let chronoM=($HC-$HD)/60   # affiche des minutes
SC2=$(date +%S --date +"$csc"seconds)	# affiche les secondes en cours avec un décalage de sdoif secondes
echo -n -e "TIMER : $chronoH : $chronoM : $SC2 \n"
echo -e '\033[44m'"CTRL+C TO STOP"
sleep 1 #.5 on peut régler le rafraîchissement
tput sgr0 # restauration des paramètres d'affichage par défaut
done
