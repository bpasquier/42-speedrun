# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    libft_creator.sh                                   :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/10/12 15:12:39 by bapasqui          #+#    #+#              #
#    Updated: 2023/10/12 15:58:21 by bapasqui         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

touch libft.a
echo "void ft_putchar(char c);" >> libft.a
echo "void ft_swap(int *a, int *b);" >> libft.a
echo "void ft_putstr(char *str);" >> libft.a
echo "int ft_strlen(char *str);" >> libft.a
echo "int ft_strcmp(char *s1, char *s2);" >> libft.a
gcc -Wall -Werror -Wextra ft_putchar.c ft_swap.c ft_putstr.c ft_strlen.c ft_strcmp.c
