/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/04 11:55:56 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/04 18:50:08 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_sqrt(int nb)
{
	int	c;
	int	d;

	c = 1;
	d = 0;
	if (nb == 0)
		return (0);
	while (nb > 0)
	{
		nb -= c;
		c = c + 2;
		d++;
	}
	return (d);
}

int	ft_is_prime(int nb)
{
	int	c;
	int	d;

	c = 2;
	d = ft_sqrt(nb);
	if (nb <= 1)
		return (0);
	if (nb == 2 || nb == 3)
		return (1);
	while (c <= d)
	{
		if (nb % c == 0)
			return (0);
		c++;
	}
	return (1);
}
