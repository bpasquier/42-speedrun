/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/03 13:54:33 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/04 14:30:42 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_factorial(int nb)
{
	int	tmp;
	int	c;

	tmp = 1;
	c = 1;
	if (nb < 0)
		return (0);
	if (nb == 0)
		return (1);
	while (c <= nb)
	{	
		tmp = tmp * c;
		c++;
	}
	return (tmp);
}
