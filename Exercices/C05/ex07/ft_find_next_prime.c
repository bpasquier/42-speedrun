/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/04 13:33:02 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/08 21:03:58 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_sqrt(int nb)
{
	int	c;
	int	d;

	c = 1;
	d = 0;
	if (nb == 0)
		return (0);
	while (nb > 0)
	{
		nb -= c;
		c = c + 2;
		d++;
	}
	return (d);
}

int	ft_is_prime(int nb)
{
	int	c;
	int	d;

	c = 2;
	d = ft_sqrt(nb);
	if (nb == 0)
		return (0);
	while (c <= d)
	{
		if (nb % c == 0)
			return (0);
		c++;
	}
	return (1);
}

int	ft_find_next_prime(int nb)
{
	if (nb <= 2)
		return (2);
	while (!ft_is_prime(nb))
		ft_is_prime(nb++);
	return (nb);
}
