/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/03 17:12:18 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/09 12:54:49 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_recursive_power(int nb, int power)
{
	if (power == 0)
		return (1);
	if (power < 0)
		return (0);
	if (power == 1)
		return (nb);
	if (power > 1)
		return (nb * ft_recursive_power(nb, power - 1));
	return (nb);
}

#include <stdio.h>
#include <stdlib.h>


int	main(int argc, char **argv)
{
	(void)argc;
	printf("%d" , ft_recursive_power(atoi(argv[1]),atoi(argv[2])));

}
