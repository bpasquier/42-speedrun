/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/25 16:11:30 by bapasqui          #+#    #+#             */
/*   Updated: 2023/09/26 21:37:11 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	ft_strupcase(char *str)
{
	int	c;

	c = 0;
	while(str[c] != '\0')
	{
		if(str[c] >= 65 && str[c] <= 90)
		{
			str[c] += 32;
		}
		c++;
	}
	return(*str);
}

int	ft_isalpha(char *str)
{
	int c;

	c = 0;
	while(str[c] != '\0')
	{
		if(str[c] >= 'a' && str[c] <= 'z')
			return (1);
		else if (str[c] >= '0' && str[c] <= '9')
			return (0);
	}
	return(1);
}

char	*ft_strcapitalize(char *str)
{
	int	c;

	c = 0;
	ft_strupcase(str);
	while (str[c] != '\0')
	{
		if (c == 0 && (str[c] >= 97 && str[c] <= 122))
		{
			str[c] -= 32;
		}
		else if ((str[c] >= 32 && str[c] <= 64)
			|| (str[c] >= 92 && str[c] <= 96)
			|| (str[c] >= 124 && str[c] <= 126))
		{
			if (str[c + 1] >= 97 && str[c + 1] <= 122)
				str[c + 1] -= 32;
			else
				c++;
		}
		c++;
	}
	return (str);
}

int	main()
{
	char c[] = "salut, comment tu vas ? 42mots quarante-deux; cinquante+et+un";
	printf("%s", ft_strcapitalize(c));
}
