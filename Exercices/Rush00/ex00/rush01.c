/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush01.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/23 11:46:44 by bapasqui          #+#    #+#             */
/*   Updated: 2023/09/24 14:50:03 by lvan-slu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(int c);

void	space(void)
{
	write(1, "\n", 1);
}

void	firstline(int x_i)
{
	int	i;

	i = 1;
	while (i <= x_i)
	{
		if (i == 1)
		{
			ft_putchar('/');
		}
		else if (i == x_i)
		{
			ft_putchar('\\');
		}
		else
		{
			ft_putchar('*');
		}
	++i;
	}
	space();
}

void	middleline(int x_i)
{
	int	i;

	i = 1;
	while (x_i >= i)
	{
		if (i == 1 || i == x_i)
		{
			ft_putchar('*');
		}
		else
		{
			write(1, " ", 1);
		}
	++i;
	}
	space();
}

void	lastline(int x_i)
{
	int	i;

	i = 1;
	while (x_i >= i)
	{
		if (i == 1)
		{
			ft_putchar('\\');
		}
		else if (i == x_i)
		{
			ft_putchar('/');
		}
		else
		{
			ft_putchar('*');
		}
	++i;
	}
	space();
}

void	rush(int x, int y)
{
	int	i;

	i = 1;
	if (x <= 0 || y <= 0)
	{
		write(1, "Error\n", 6);
		return ;
	}	
	while (y >= i)
	{
		if (i == 1)
		{
			firstline(x);
		}
		else if (i == y)
		{
			lastline(x);
		}
		else
		{
			middleline(x);
		}
	++i;
	}
}
