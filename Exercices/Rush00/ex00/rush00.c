/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/23 11:46:44 by bapasqui          #+#    #+#             */
/*   Updated: 2023/09/23 17:47:41 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(int c);

void	space(void)
{
	write(1, "\n", 1);
}

void	firstline(int x_i)
{
	int	i;

	i = 0;
	while (i <= x_i - 1)
	{
		if (i == 0 || i == x_i - 1)
		{
			ft_putchar('o');
		}
		else
		{
			ft_putchar('-');
		}
	++i;
	}
	space();
}

void	middleline(int x_i)
{
	int	i;

	i = 0;
	while (x_i - 1 >= i)
	{
		if (i == 0 || i == x_i - 1)
		{
			ft_putchar('|');
		}
		else
		{
			write(1, " ", 1);
		}
	++i;
	}
	space();
}

void	lastline(int x_i)
{
	int	i;

	i = 0;
	while (x_i - 1 >= i)
	{
		if (i == 0 || i == x_i - 1)
		{
			ft_putchar('o');
		}
		else
		{
			ft_putchar('-');
		}
	++i;
	}
	space();
}

void	rush(int x, int y)
{
	int	i;

	i = 0;
	if (x <= 0 || y <= 0)
	{
		write(1, "Error\n", 6);
	}	
	while (y - 1 >= i)
	{
		if (i == 0)
		{
			firstline(x);
		}
		else if (i == y - 1)
		{
			lastline(x);
		}
		else
		{
			middleline(x);
		}
	++i;
	}
}
