/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llahaye <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/10 11:00:12 by llahaye           #+#    #+#             */
/*   Updated: 2023/10/11 20:08:15 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "bsq.h"

void	ft_read(char *buffer)
{
	char	tmp[350];
	int		i;

		i = 0;
	while (buffer[i] != '\0')
	{
		tmp[i] = buffer[i];
		i++;
	}
	tmp[i] = '\0';
	ft_putstr(tmp);
}

int	main(int argc, char **argv)
{
	int		i;
	char	buffer[350];
	t_map	map;

	if (argc - 1 == 0)
	{	
		read (0, &buffer, 350);
		ft_read(buffer);
	}
	else if (argc > 1)
	{
		i = 1;
		while (i < argc)
		{
			map = ft_define_map_struct(ft_parse_map(argv[i]));
			ft_init_grid(&map);
			if (check_all(map) == 1)
			{	
				ft_display_grid(map);
				free(map.grid);
			}
			i++;
		}
	}
}
