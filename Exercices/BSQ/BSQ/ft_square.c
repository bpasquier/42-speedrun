/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_square.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llahaye <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/11 14:40:48 by llahaye           #+#    #+#             */
/*   Updated: 2023/10/11 18:17:17 by llahaye          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"
#include <stdio.h>

void	ft_init_square(t_square *square, int bline, int bcolumn)
{
	square->begin_line = bline;
	square->begin_column = bcolumn;
	square->end_line = bline;
	square->end_column = bcolumn;
}

int	ft_square_size(t_square *square)
{
	return (square->end_line - square->begin_line);
}

void	ft_define_biggest(t_square *biggest, t_square *square)
{
	if (ft_square_size(square) > ft_square_size(biggest))
	{
		biggest->begin_line = square->begin_line;
		biggest->begin_column = square->begin_column;
		biggest->end_line = square->end_line;
		biggest->end_column = square->end_column;
	}
}
