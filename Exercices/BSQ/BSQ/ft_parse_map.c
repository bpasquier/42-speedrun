/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_map.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llahaye <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/10 10:58:10 by llahaye           #+#    #+#             */
/*   Updated: 2023/10/11 18:00:34 by llahaye          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

char	*ft_parse_map(char *filename)
{
	int		fd;
	int		bytesread;
	char	*map_content;
	char	buffer[1024];

	fd = open(filename, O_RDWR);
	if (fd == -1)
	{
		exit(0);
	}
	bytesread = read(fd, buffer, sizeof(buffer));
	map_content = malloc(sizeof(char *) * bytesread);
	if (!(map_content))
		return (0);
	map_content = buffer;
	close(fd);
	return (map_content);
}

t_map	ft_define_map_struct(char *map_content)
{
	int		i;
	int		j;
	char	*map_first_line;
	t_map	map;

	i = 0;
	map_first_line = ft_get_first_line(map_content);
	map.lines = 0;
	while (ft_is_digit(map_first_line[i]))
	{
		map.lines = 10 * map.lines + map_first_line[i] - '0';
		i++;
	}
	map.empty = map_first_line[i];
	map.obstacle = map_first_line[i + 1];
	map.full = map_first_line[i + 2];
	map.first_line_size = i + 3;
	map.content = map_content;
	j = i + 4;
	while (map_content[j] != '\n')
		j++;
	map.columns = j - 4;
	free(map_first_line);
	return (map);
}

char	*ft_get_first_line(char *map_content)
{
	int		i;
	char	*first_line;

	i = 0;
	while (map_content[i] != '\n')
	{
		i++;
	}
	first_line = malloc(sizeof(char) * i);
	if (!(first_line))
		return (0);
	i = 0;
	while (map_content[i] != '\n')
	{
		first_line[i] = map_content[i];
		i++;
	}
	return (first_line);
}
