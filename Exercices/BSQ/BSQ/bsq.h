/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llahaye <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/11 11:48:04 by llahaye           #+#    #+#             */
/*   Updated: 2023/10/11 20:08:55 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BSQ_H
# define BSQ_H
# include <unistd.h>
# include <stdlib.h>

typedef struct t_map
{
	int		lines;
	int		columns;
	char	empty;
	char	obstacle;
	char	full;
	char	*content;
	char	**grid;
	int		first_line_size;
}	t_map;

typedef struct t_square
{
	int	begin_line;
	int	begin_column;
	int	end_line;
	int	end_column;
}	t_square;

void		ft_init_square(t_square *square, int bline, int bcolumn);
void		ft_define_biggest(t_square *biggest, t_square *square);
void		ft_expand(t_map map, t_square *square);
void		ft_display_grid(t_map map);
void		ft_init_grid(t_map *map);
void		ft_putchar(char c);
void		ft_putstr(char *str);
t_map		ft_define_map_struct(char *map_content);
t_square	ft_solve(t_map map);
char		*ft_parse_map(char *filename);
char		*ft_get_first_line(char *map_content);
int			ft_atoi(char *str);
int			ft_is_digit(char c);
int			ft_check_obs(t_map map, t_square *square);
int			ft_square_size(t_square *square);
int			check_lines(t_map map);
int			check_char(t_map map);
int			check_all(t_map map);
#endif
