/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_solve.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llahaye <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/11 13:40:47 by llahaye           #+#    #+#             */
/*   Updated: 2023/10/11 18:16:41 by llahaye          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

t_square	ft_solve(t_map map)
{
	int			row;
	int			col;
	t_square	biggest;
	t_square	square;

	row = 0;
	col = 0;
	ft_init_square(&biggest, 0, 0);
	while (row < map.lines)
	{
		while (col < map.columns)
		{
			if (map.grid[row][col] != map.obstacle)
			{
				ft_init_square(&square, row, col);
				ft_expand(map, &square);
				ft_define_biggest(&biggest, &square);
			}
			col++;
		}
		row++;
		col = 0;
	}
	return (biggest);
}

int	ft_check_obs(t_map map, t_square *square)
{
	int	row;
	int	col;

	row = square->begin_line;
	col = square->begin_column;
	while (row < square->end_line)
	{
		while (col < square->end_column)
		{
			if (map.grid[row][col] == map.obstacle)
				return (0);
			col++;
		}
		row++;
		col = square->begin_column;
	}
	return (1);
}

void	ft_expand(t_map map, t_square *square)
{
	while (ft_check_obs(map, square) && square->end_line < map.lines)
	{
		square->end_line += 1;
		square->end_column += 1;
	}
}
