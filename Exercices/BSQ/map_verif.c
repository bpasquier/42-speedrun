/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_verif.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/11 12:05:09 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/11 17:02:21 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int	check_lines(t_map map)
{
	int	c;
	int d;

	d = 0;
	c = 0;
	while (c < map.lines)
	{
		while(map.grid[c][d] != '\n')
			d++;                                                        	
		if (!(d == map.columns))
			return (0);
		d = 0;
		c++;
	}
	return (1);
}

int	check_char(t_map map)
{
	int	c;
	int	d;

	c = 0;
	d = 0;	
	while(c < map.lines)
	{
		while(map.grid[c][d] != '\n')
		{
			if(map.grid[c][d] != map.empty)
				return (1);
			d++;
		}
		d = 0;
		c++;
	}
	return (0);
}
