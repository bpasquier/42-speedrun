/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llahaye <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/11 11:48:04 by llahaye           #+#    #+#             */
/*   Updated: 2023/10/11 15:35:55 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BSQ_H
# define BSQ_H
# include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct t_map
{
	int		lines;
	int		columns;
	char	empty;
	char	obstacle;
	char	full;
	char	*content;
	char	**grid;
	int		first_line_size;
}	t_map;

//FT_PARSE_MAP
char	*ft_parse_map(char *filename);
t_map	ft_define_map_struct(char *map_content);
char	*ft_get_first_line(char *map_content);

//FT.C
int		ft_atoi(char *str);
int		ft_is_digit(char c);
void	ft_putchar(char c);

//FT_GRID.C
void	ft_init_grid(t_map *map);
void	ft_display_grid(t_map map);

//MAP_VERIF
int check_lines(t_map map);
int check_char(t_map map);
void ft_error();
#endif
