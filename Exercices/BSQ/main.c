/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llahaye <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/10 11:00:12 by llahaye           #+#    #+#             */
/*   Updated: 2023/10/11 17:47:29 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "bsq.h"

int	main(int argc, char **argv)
{
	int i;
	int		fd = 0;
	char	buffer[350];
	if (argc - 1 == 0)
	{	
		read(fd, &buffer, 350);
		t_map map = ft_define_map_struct(ft_parse_map(buffer));
		printf("%s", buffer);
		ft_init_grid(&map);
		ft_display_grid(map);
	}
	else if (argc >= 1)
	{
		i = 1;
		while (i < argc)
		{
			t_map map = ft_define_map_struct(ft_parse_map(argv[i]));
			ft_init_grid(&map);
			ft_display_grid(map);
			ft_putchar('\n');
			i++;
		}
		return (0);
	}
}

