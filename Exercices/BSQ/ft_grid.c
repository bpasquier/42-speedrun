/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_grid.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llahaye <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/11 00:14:22 by llahaye           #+#    #+#             */
/*   Updated: 2023/10/11 12:28:56 by llahaye          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"
#include <stdlib.h>
#include <stdio.h>

void	ft_display_grid(t_map map)
{
	int	row;
	int	col;

	row = 0;
	col = 0;
	while (row < map.lines)
	{
		while (col < map.columns)
		{
			ft_putchar(map.grid[row][col]);
			col++;
		}
		row++;
		col = 0;
	}
}

void	ft_init_grid(t_map *map)
{
	int	row;
	int	col;
	int	i;

	i = map->first_line_size + 1;
	row = 0;
	col = 0;
	map->grid = malloc(sizeof(char **) * map->lines);
	while (row < map->lines)
	{
		map->grid[row] = malloc(sizeof(char) * map->columns + 1);
		while (col < map->columns)
		{
			map->grid[row][col] = map->content[i];
			i++;
			col++;
		}
		row++;
		col = 0;
	}
}
