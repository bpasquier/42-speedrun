/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_params.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/01 21:16:18 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/02 20:33:37 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	main(int argc, char **argv)
{
	int	c;
	int	d;
	int	e;

	c = 0;
	d = 0;
	e = (argc - 1);
	while (e != c)
	{
		while (argv[e][d] != '\0')
		{
			write(1, &argv[e][d], 1);
			d++;
		}
		write(1, "\n", 1);
		d = 0;
		e--;
	}
}
