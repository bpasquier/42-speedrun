/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/01 21:01:49 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/02 20:32:44 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	main(int argc, char **argv)
{
	int	c;
	int	d;

	c = 1;
	d = 0;
	while (c != argc)
	{
		while (argv[c][d] != '\0')
		{
			write(1, &argv[c][d], 1);
			d++;
		}
		write(1, "\n", 1);
		d = 0;
		c++;
	}
}
