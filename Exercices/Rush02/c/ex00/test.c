/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/07 14:17:09 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/08 16:26:15 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int	user_input(int an,char **in);

void ft_putstr(char *str)
{
	int	a;

	a = 0;
	while (!(str[a] >= 'a' && str[a] <= 'z'))
		a++;
	while (str[a] >= 'a' && str[a] <= 'z' && str[a] != 10)
	{
		write (1, &str[a], 1);
		a++;
	}
}


int	ft_strncmp(char *s1, char *s2, unsigned int n)
{
	unsigned int	a;
	unsigned int	b;

	a = 0;
	b = 0;
	while ((s1[a] != '\0' || s2[a] != '\0') && b < n)
	{
		if (s1[a] != s2[a])
			return (s1[a] - s2[a]);
		a++;
		b++;
	}
	return (0);
}

int ft_strlen(char *str)
{
	int	c;
	
	c = 0;
	while (str[c]  != '\0')
		c++;
	return (c);
}
char	*ft_strcat(char *dest, char *src)
{
	int	c;
	int	d;

	c = 0;
	d = 0;
	while (dest[d] != '\0')
	{
		d++;
	}
	while (src[c] != '\0')
	{
		dest[d] = src[c];
		d++;
		c++;
	}
	dest[d] = '\0';
	return (dest);
}

char	*ft_strstr_digits(char *str, char *to_find)
{
	int	c;
	int	d;

	c = 0;
	d = 0;
	if (to_find[0] == '\0' || to_find == 0)
		return (str);
	if (str == 0)
		return (0);
	while (str[c] != '\0')
	{
		while (str[c + d] == to_find[d] && str[c + d] != '\n')
		{
			d++;
			if (to_find[d] == '\0')
				ft_putstr(&str[c]);
		}
			c++;
	}
	return (0);
}

char	*ft_strstr_numbers(char *str, char *to_find)
{
	int	i;
	int	j;

	i = 0;
	if (to_find[0] == '\0')
		return (str);
	while (str[i] != '\0')
	{
		j = 0;
		while (str[i + j] != '\0' && str[i + j] == to_find[j])
		{
			if (to_find[j + 1] == '\0')
				ft_putstr(&str[i]);
			j++;
		}
		i++;
	}
	return (0);
}

int	ft_atoi(char *str)
{
	int		c;
	int		d;
	int		e;
	char	oe;

	c = 0;
	oe = 1;
	d = 0;
	e = 0;
	while (str[c] == 32 || (str[c] >= 9 && str[c] <= 13))
		c++;
	while (str[c] == 45 || str[c] == 43)
	{
		if (str[c] == 45)
			d++;
		c++;
	}
	if (d % 2)
		oe = -1;
	while (str[c] >= '0' && str[c] <= '9')
	{
		e = e * 10 + str[c] - '0';
		c++;
	}
	return (e * oe);
}
void	ft_putchar(char c)
{
	write(1, &c, 1);
}
int	main(int argc, char **argv)
{
	int fd;
	char buffer[350];
	char *filename = "numbers.dict";

	fd = open(filename, O_RDONLY);

	if (fd == -1)
		write(1, "error\n", 6);
	read(fd, &buffer, 350);
		
	if (user_input(argc, argv) == 1)
	{
		if (ft_atoi(argv[1]) >= 0 && ft_atoi(argv[1]) <= 10)
			ft_strstr_digits(buffer, argv[1]);
		else if (ft_atoi(argv[1]) >= 11 && ft_atoi(argv[1]) <= 90)
			ft_strstr_numbers(buffer, argv[1]);	
	}
	else
		write(1, "Error\n", 6);
	close(fd);	
}
