/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bturcios <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/07 08:08:34 by bturcios          #+#    #+#             */
/*   Updated: 2023/10/07 19:20:48 by rbouzerb         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int	user_input(int an, char **in);

int	ft_atoi(char *str)
{
	unsigned long long	i;
	unsigned long long	sing;
	unsigned long long	rest;

	i = 0;
	sing = 1;
	rest = 0;
	while (str[i] == 32 || (str[i] >= 9 && str[i] <= 13))
		i++;
/*	while (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			sing = sing * (-1);
		i++;
	}*/
	while (str[i] >= '0' && str[i] <= '9')
	{		
		rest = rest * 10 + (str[i] - '0');
		i++;
	}
	return (rest * sing);
}

int	main(int argc, char *argv[])
{	
	unsigned long long int	x;

	(void)argc;
	if (ft_atoi(argv[1]) < 0)
	{
		write(1, "Error\n", 6);
		return (0);
	}
	x = ft_atoi(argv[1]);
	printf("%llu", x);
	return (0);
}
