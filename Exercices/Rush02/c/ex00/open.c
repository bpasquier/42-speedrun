/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   open.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qserbu <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/07 10:33:56 by qserbu            #+#    #+#             */
/*   Updated: 2023/10/07 19:24:45 by rbouzerb         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int	main(void)
{
	int		a;
	char	c[350];

	a = open("numbers.dict", O_RDONLY);
//	printf("opened the file = %d\n", a);
	read(a, &c, 15);
	printf("%s\n", c);
	close(a);
//	printf("closed the file\n");
	return (0);
}
