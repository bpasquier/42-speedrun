/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   extract.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbouzerb <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/08 17:20:33 by rbouzerb          #+#    #+#             */
/*   Updated: 2023/10/08 18:48:05 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

char	*ft_strstr_digits(char *str, char *to_find)
{
	int	c;
	int	d;

	c = 0;
	d = 0;
	if (to_find[0] == '\0' || to_find == 0)
		return (str);
	if (str == 0)
		return (0);
	while (str[c] != '\0')
	{
		while (str[c + d] == to_find[d] && str[c + d] != '\n')
		{
			d++;
			if (to_find[d] == '\0')
				ft_putstr(&str[c]);
		}
			c++;
	}
	return (0);
}

char	*ft_strstr_numbers(char *str, char *to_find)
{
	int	i;
	int	j;

	i = 0;
	if (to_find[0] == '\0')
		return (str);
	while (str[i] != '\0')
	{
		j = 0;
		while (str[i + j] != '\0' && str[i + j] == to_find[j])
		{
			if (to_find[j + 1] == '\0')
				ft_putstr(&str[i]);
			j++;
		}
		i++;
	}
	return (0);
}
