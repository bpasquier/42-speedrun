/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/08 18:36:11 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/08 19:37:10 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_H
# define HEADER_H

# include <unistd.h>
# include <fcntl.h>

int		user_input(int an, char **in);
void	ft_putstr(char *str);
char	*ft_strstr_digits(char *str, char *to_find);
char	*ft_strstr_numbers(char *str, char *to_find);
void	ft_pustr(char *str);
int		ft_atoi(char *str);

#endif
