/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bturcios <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/07 08:08:34 by bturcios          #+#    #+#             */
/*   Updated: 2023/10/08 19:35:29 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int	main(int argc, char **argv)
{
	int		fd;
	char	buffer[350];
	char	*filename;

	filename = "numbers.dict";
	fd = open(filename, O_RDONLY);
	if (fd == -1)
		write(1, "Dict Error\n", 11);
	read(fd, &buffer, 350);
	if (user_input(argc, argv) == 1)
	{
		if (ft_atoi(argv[1]) >= 0 && ft_atoi(argv[1]) <= 10)
			ft_strstr_digits(buffer, argv[1]);
		else if (ft_atoi(argv[1]) >= 11 && ft_atoi(argv[1]) <= 90)
			ft_strstr_numbers(buffer, argv[1]);
	}
	else
		write(1, "Dict Error\n", 11);
	close(fd);
}
