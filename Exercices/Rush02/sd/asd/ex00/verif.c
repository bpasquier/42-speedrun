/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verif.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/07 11:25:00 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/08 19:43:35 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strlen(char *str)
{
	int	c;

	c = 0;
	while (str[c] != '\0')
		c++;
	return (c);
}

int	user_input(int an, char **in)
{
	int	c;
	int	d;

	c = 0;
	d = 0;
	if (an - 1 == 1 || an - 1 == 2)
	{
		while (in[1][c] != '\0')
		{
			if ((in[1][c] >= 33 && in[1][c] <= 47) \
			|| (in[1][c] >= 58 && in[1][c] <= 126))
				d++;
			else if (ft_strlen(&in[1][c]) > 13)
				d++;
			c++;
		}
	}
	else
		return (0);
	if (d == 0)
		return (1);
	return (0);
}
