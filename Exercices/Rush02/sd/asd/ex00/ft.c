/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbouzerb <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/08 16:28:58 by rbouzerb          #+#    #+#             */
/*   Updated: 2023/10/08 18:55:52 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	ft_putstr(char *str)
{
	int		a;

	a = 0;
	while (str[a])
	{
		while (!(str[a] >= 'a' && str[a] <= 'z'))
			a++;
		while (str[a] >= 'a' && str[a] <= 'z')
		{
			write (1, &str[a], 1);
			a++;
		}
		if (str[a] == 10)
			return ;
	}
}

int	ft_atoi(char *str)
{
	int		c;
	int		d;
	int		stock;
	char	oe;

	c = 0;
	oe = 1;
	d = 0;
	stock = 0;
	while (str[c] == 32 || (str[c] >= 9 && str[c] <= 13))
		c++;
	while (str[c] == 45 || str[c] == 43)
	{
		if (str[c] == 45)
			d++;
		c++;
	}
	if (d % 2)
		oe = -1;
	while (str[c] >= '0' && str[c] <= '9')
	{
		stock = stock * 10 + str[c] - '0';
		c++;
	}
	return (stock * oe);
}
