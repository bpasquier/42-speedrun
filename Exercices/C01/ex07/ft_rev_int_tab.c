/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_int_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/22 11:08:54 by bapasqui          #+#    #+#             */
/*   Updated: 2023/09/24 16:14:44 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_rev_int_tab(int *tab, int size)
{
	int	tmp;
	int	i;
	int	st
	[size];

	i = 0;
	tmp = size - 1 ;
	while (i <= size - 1)
	{
		st[i] = tab[i];
		i++;
	}
	i = 0;
	while (i <= size - 1)
	{
		tab[i] = st[tmp];
		i++;
		tmp--;
	}
}
