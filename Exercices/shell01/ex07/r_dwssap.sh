# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    r_dwssap.sh                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/10/10 19:06:30 by bapasqui          #+#    #+#              #
#    Updated: 2023/10/10 19:15:52 by bapasqui         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#!/bin/sh
export FT_LINE1=2
export FT_LINE2=7    

cat /etc/passwd | \
    grep -v '\#' | \
    sed '1!n;d' | \
    cut -d':' -f1 | \
    rev | \
    sort -r | \
    awk 'NR>= ENVIRON["FT_LINE1"] && NR<= ENVIRON["FT_LINE2"]' | \
    paste -s -d"," - | \
    sed 's/,/, /g' | \
    sed 's/$/./' | \
    tr -d '\n'

