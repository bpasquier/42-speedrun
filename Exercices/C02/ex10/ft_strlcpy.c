/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/26 12:02:18 by bapasqui          #+#    #+#             */
/*   Updated: 2023/09/26 19:28:44 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
int	ft_strlen(char *str)
{
	int	c;

	c = 0;
	while (str[c] != '\0')
	{
		c++;
	}
	return (c);
}

unsigned	int	ft_strlcpy(char *dest, char *src, unsigned int size)
{
	unsigned int	c;

	c = 0;
	size = ft_strlen(src);
	while (src[c] != '\0' c < size)
	{
		dest[c] = src[c];
		c++;
	}	
	while (c < size)
	{
		dest[c] = '\0';
		c++;
	}
	return (size);
}
