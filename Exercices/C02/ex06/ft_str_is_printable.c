/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_printable.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/25 15:44:52 by bapasqui          #+#    #+#             */
/*   Updated: 2023/09/26 19:14:45 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_str_is_printable(char *str)
{
	int	c;
	int	b;

	b = 0;
	c = 0;
	while (str[c] != '\0')
	{
		if ((str[c] >= 32 && str[c] <= 126))
		{
		b++;
		}
		c++;
	}
	if (b == c || c == 0)
	{
		return (1);
	}
	else
	{
		return (0);
	}
}
