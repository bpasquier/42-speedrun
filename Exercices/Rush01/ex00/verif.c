/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verif.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/30 11:03:09 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/01 19:52:50 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	check_four(char **argv)
{
	int	d;
	int	c;

	d = 1;
	c = 0;
	while (argv[d][c] != argv[d][26])
	{
		if (argv[d][c] == 4 && argv[d][c + 4] != 1)
			return (0);
		d++;
	}
	return (1);
}

int	check_size(char **argv)
{
	int	d;

	d = 1;
	while (argv[1][d] != '\0')
	{
		if (argv[1][d] > 4)
			return (0);
		d++;
	}
	return (1);
}

int	check_resolv(char **argv)
{
	int	c;
	int	d;

	d = 0;
	c = 0;
	while (argv[1][c] != '\0')
	{
		d++;
		c++;
	}
	if (d > 17)
		return (0);
	return (1);
}

int	check_value(char **argv)
{
	int	c;
	int	d;

	c = 0;
	d = 0;
	while (argv[1][c] != '\0')
	{
			d = argv[1][c] + d;
			c++;
	}
	if (d != 34)
		return (0);
	return (1);
}
