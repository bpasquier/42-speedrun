/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_column.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tappourc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/01 16:57:42 by tappourc          #+#    #+#             */
/*   Updated: 2023/10/01 19:49:52 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
int	tab_check_column(int **tab[3][3]);
int validity_column(int a, int b, int c, int d, int input)

int tab_check(int **tab[4][4])
{
	int ok;

	ok = 0;
	if (validity_column(tab[0][0], tab[1][0], 
tab[2][0], tab[3][0], col1up))
		ok++;
	else if (validity_column(tab[0][1], tab[1][1], 
tab[2][1], tab[3][1], col2up))
		ok++;
	else if (validity_column(tab[0][2], tab[1][2], 
tab[2][2], tab[3][2], col3up))
		ok++;
	else if (validity_column(tab[0][3], tab[1][3], 
tab[2][3], tab[3][3], col4up))
		ok++;
	else if (validity_column(tab[3][0], tab[2][0], 
tab[1][0], tab[0][0], col1down))
		ok++;
	else if (validity_column(tab[3][1], tab[2][1], 
tab[1][1], tab[0][1], col2down))
		ok++;
	else if (validity_column(tab[3][2], tab[2][2], 
tab[1][2], tab[0][2], col3down))
		ok++;
	else if (validity_column(tab[3][3], tab[2][3], tab[1][3],
 tab[0][3], col4down))
		ok++;
	if (ok == 8)
	return (0);
	else
	return (1);
}
*/
int	validity_column(int a, int b, int c, int d)
{
	int	count;
	int	input;

	count = 1;
	input = 1;
	if (a < b)
		count++;
	if (b < c)
		count++;
	if (c < d)
		count++;
	if (count == input)
		return (0);
	else
		return (1);
}
