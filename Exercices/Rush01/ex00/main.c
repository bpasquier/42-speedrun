/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/01 14:46:27 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/01 20:12:15 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	checking(void);
//void print_rows(void);

int	main(void)
{
	if (checking() == 1)
		write(1, "print_rows", 10);
	else
		write (1, "Error\n", 6);
}
