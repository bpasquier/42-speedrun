/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checking.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/01 14:00:27 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/01 17:57:53 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	check_size(int argc, char **argv);
//int check_resolv(int argc, char **argv);
//int	check_four(int argc, char **argv);
int	in_verif(int argc, char **argv);

int	checking(int argc, char **argv)
{
	if (in_verif(argc, argv) == 1)
		return (1);
	return (0);
}
