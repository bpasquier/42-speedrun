/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_lines.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tappourc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/01 17:04:17 by tappourc          #+#    #+#             */
/*   Updated: 2023/10/01 20:03:56 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
int	tab_check_line(int **tab[3][3]);
int	check_left_to_right(int *tab[3], int input);
int	check_right_to_left(int *tab[3], int input);
int	validity_line(int *tab[3], int left, int right);

int	tab_check(int **tab[3][3])
{
	int	ok;

	ok = 0;
	if (validity_line(tab[0], int row1left, int row1right))
		ok++;
	if (validity_line(tab[1], int row2left, int row2right))
		ok++;
	if (validity_line(tab[2], int row3left, int row3right))
		ok++;
	if (validity_line(tab[3], int row4left, int row4right))
		ok++;
	if (ok == 4)
		return (0);
	else
		return (1);
}

int	validity_line(int *tab[3], int left, int right)
{
	int	ok;

	ok = 0;
	if (check_left_to_right, left)
		ok++;
	if (check_right_to_left, right)
		ok++;
	if (ok == 2)
		return (0);
	else
		return (1);
}

int	check_left_to_right(int *tab[3], int input)
{
	int	count;
	int	rank;

	count = 1;
	rank = 0;
	while (rank <= 3)
	{
		if (tab[rank] < tab[rank + 1])
			count++;
		rank++;
	}
	if (count == input)
		return (0);
	else
		return (1);
}

int	check_right_to_left(int *tab[3], int input)
{
	int	count;
	int	rank;

	count = 1;
	rank = 3;
	while (rank >= 0)
	{
		if (tab[rank] < tab[rank - 1])
			count++;
		rank++;
	}
	if (count == input)
		return (0);
	else
		return (1);
}
*/
