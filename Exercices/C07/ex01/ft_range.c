/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2005/10/05 14:37:17 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/12 12:31:01 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_range(int min, int max)
{
	int	c;
	int	*range;

	if (min >= max)
		return (0);
	c = max - min;
	range = malloc(sizeof(int) * c);
	if (range == NULL)
		return (NULL);
	c = 0;
	while (min < max)
	{
		range[c] = min;
		min++;
		c++;
	}
	free(range);
	return (range);
}
