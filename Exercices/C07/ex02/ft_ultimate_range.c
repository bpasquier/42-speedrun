/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/05 14:16:51 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/12 12:32:05 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	ft_ultimate_range(int **range, int min, int max)
{
	int	c;

	if (min >= max)
	{
		*range = NULL;
		return (0);
	}
	c = max - min;
	*range = malloc(sizeof(int) * c);
	c = 0;
	while (min < max)
	{
		range[0][c] = min;
		min++;
		c++;
	}
	return (min - max);
}
