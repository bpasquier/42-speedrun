/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bapasqui <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/05 14:16:51 by bapasqui          #+#    #+#             */
/*   Updated: 2023/10/12 12:27:03 by bapasqui         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	ft_strlen_arg(int ac, char **car)
{
	int	c;
	int	d;
	int	count;

	c = 0;
	d = 0;
	count = 0;
	while (c != ac)
	{
		while (car[c][d] != '\0')
		{
			d++;
			count++;
		}
		d = 0;
		c++;
	}
	return (count);
}

int	ft_strlen_sep(char *str)
{
	int	c;

	c = 0;
	while (str[c] != '\0')
		c++;
	return (c);
}

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	unsigned int	c;

	c = 0;
	while (src[c] != '\0' && c < n)
	{
		dest[c] = src[c];
		c++;
	}
	while (c < n)
	{
		dest[c] = '\0';
		c++;
	}
	return (dest);
}

char	*ft_strcat(int an, char **av, char *sep, char *cr)
{
	int	c;
	int	d;
	int	e;
	int	w;

	w = 0;
	c = 0;
	d = 0;
	e = 0;
	while (e != an)
	{
		while (av[e][d] != '\0')
			cr[w++] = av[e][d++];
		d = 0;
		while (sep[c] != '\0')
			cr[w++] = sep[c++];
		c = 0;
		e++;
	}
	cr[w] = '\0';
	return (cr);
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	int		i;
	int		j;
	char	*cr;
	char	*rt;
	char	*dest;

	i = 0;
	if (size == 0)
	{
		rt = malloc(0);
		return (rt);
	}
	i = ft_strlen_sep(sep);
	j = ft_strlen_arg(size, strs);
	cr = malloc(sizeof(char *) * j * i * size + 1);
	dest = malloc(sizeof(char *) * j * i * size + 1);
	cr = ft_strcat(size, strs, sep, cr);
	ft_strncpy(dest, cr, ft_strlen_sep(cr) - 1);
	return (dest);
}
